pipeline{
    agent any
    environment{
        REGISTRY = "registry.gitlab.com"
        GIT_REPO = "https://gitlab.com/asidoasif/cowsay-pipeline.git"
        IMAGE_TAG = "1.0"
        IMAGE_NAME = "cowsay"
        API_TOKEN = credentials('Gitlab_API_Token_Text')
    }
    stages{
        stage("Builds Cowsay's image"){
            when{
                anyOf{
                    branch 'main'
                    branch 'staging'
                    branch "feature/*"
                }
            }
            steps{
                echo "======== Executing Builds Cowsay's image ========"
                sh "docker build -t ${IMAGE_NAME}:${BUILD_NUMBER} ."
                sh "docker tag ${IMAGE_NAME}:${BUILD_NUMBER} ${IMAGE_NAME}:latest"
                echo "======== Finish To Builds Cowsay's image ========"
            }
        }
        stage("Publish To Registry"){
            steps{
                echo "======== Start Publish Cowsay's ========"
                withCredentials([string(credentialsId: 'Gitlab_API_Token_Text', variable: 'API_TOKEN')]) {
                    sh 'docker login registry.gitlab.com -u asidoasif -p${API_TOKEN}'
                    sh 'docker build -t registry.gitlab.com/asidoasif/cowsay-pipeline .'
                    sh 'docker push registry.gitlab.com/asidoasif/cowsay-pipeline:latest'
                }
                echo "======== Finish Publish Cowsay's ========"
            }
        }
        stage('Deploy') {
            steps {
                echo "======== Start Deploy Cowsay's ========"
                echo "Branch ${env.BRANCH_NAME}"
                
                script {
                    PORT = '8081'
                    prefix = "feature/"
                    
                    if (env.BRANCH_NAME.startsWith(prefix)) {
                        sh 'docker run -d -p ${PORT}:8080 --name cowsay_pipeline_feature ${IMAGE_NAME}:${BUILD_NUMBER}'
                    } else {
                        def CONTAINER_NAME = ""
                        if (env.BRANCH_NAME == "main") {
                            PORT = '80'
                            CONTAINER_NAME = "cowsay_pipeline_main"
                        } else if (env.BRANCH_NAME == "staging") {
                            PORT = '3000'
                            CONTAINER_NAME = "cowsay_pipeline_staging"
                        }
                        sshagent(['ec2-deploy-key']) {
                            withCredentials([string(credentialsId: 'Gitlab_API_Token_Text', variable: 'API_TOKEN')]) {
                                // add stop container if already running - main
                                sh """
                                ssh -o StrictHostKeyChecking=no ubuntu@ec2-3-238-152-18.compute-1.amazonaws.com '
                                    docker login registry.gitlab.com -u asidoasif -p${API_TOKEN} && \
                                    docker pull registry.gitlab.com/asidoasif/cowsay-pipeline:latest && \
                                    docker stop ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}
                                    docker run -d -p ${PORT}:8080 --name ${CONTAINER_NAME} registry.gitlab.com/asidoasif/cowsay-pipeline:latest
                                '
                                """
                            }
                        }
                        sh 'docker rmi ${IMAGE_NAME}:${BUILD_NUMBER}'
                    }
                }
                echo "======== Finish Deploy Cowsay's ========"
            }
        }
        stage("Testing Cowsay's"){
            steps{
                echo "======== Start Testing Cowsay's ========"
                script {
                    def PORT = '8081'
                    def prefix = "feature/"
                    if (env.BRANCH_NAME.startsWith(prefix)) {
                        sh("""
                        until curl --include http://ec2-34-232-105-116.compute-1.amazonaws.com:${PORT}/ | head -n 1 | grep '200 OK'
                        do
                            sleep 1
                        done
                        docker stop cowsay_pipeline_feature && docker rm cowsay_pipeline_feature
                        docker rmi ${IMAGE_NAME}:${BUILD_NUMBER}
                        """) 
                    }
                    else{
                    if (env.BRANCH_NAME == "main") {
                        PORT = '80'
                    } else if (env.BRANCH_NAME == "staging") {
                        PORT = '3000'
                    }
                    sh("""
                    until curl --include http://ec2-3-238-152-18.compute-1.amazonaws.com:${PORT}/ | head -n 1 | grep '200 OK'
                    do
                        sleep 1
                    done
                    """) 
                echo "========Finish To Builds Cowsay's image========"
                }
                }
            }
        }
    }
    post{
        always{
            echo "========always========"
            cleanWs()
        }
        success{
            echo "========pipeline executed successfully ========"
            script {
                if (env.GIT_COMMITTER_EMAIL) {
                    mail to: env.GIT_COMMITTER_EMAIL,
                         subject: "SUCCESS: Build ${env.BUILD_TAG}",
                         body: "Build ${env.BUILD_TAG} succeeded."
                } else {
                    mail to: 'default-email@example.com',
                         subject: "SUCCESS: Build ${env.BUILD_TAG}",
                         body: "Build ${env.BUILD_TAG} succeeded."
                }
            }
        }
        failure{
            echo "========pipeline execution failed========"
            script {
                if (env.GIT_COMMITTER_EMAIL) {
                    mail to: env.GIT_COMMITTER_EMAIL,
                         subject: "FAILURE: Build ${env.BUILD_TAG}",
                         body: "Build ${env.BUILD_TAG} failed."
                } else {
                    mail to: 'default-email@example.com',
                         subject: "FAILURE: Build ${env.BUILD_TAG}",
                         body: "Build ${env.BUILD_TAG} failed."
                }
            }
        }
    }
}